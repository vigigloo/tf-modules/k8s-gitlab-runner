# GitLab runners for groups and projects

Deploys GitLab runners to a Kubernetes cluster and configures them for groups and projects.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | n/a |
| <a name="provider_helm"></a> [helm](#provider\_helm) | n/a |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [helm_release.gitlab-runner-group](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [helm_release.gitlab-runner-project](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [kubernetes_namespace.gitlab-runner](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace) | resource |
| [kubernetes_secret.users](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [gitlab_group.group](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/group) | data source |
| [gitlab_project.project](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/project) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cache_provider"></a> [cache\_provider](#input\_cache\_provider) | What provider to use for caching files from CI jobs. Either empty or `s3`. | `string` | `""` | no |
| <a name="input_cache_s3_access-key"></a> [cache\_s3\_access-key](#input\_cache\_s3\_access-key) | Access key for the S3 cache. | `string` | `""` | no |
| <a name="input_cache_s3_bucket-name"></a> [cache\_s3\_bucket-name](#input\_cache\_s3\_bucket-name) | Name of the bucket where the S3 cache should be persisted. | `string` | `""` | no |
| <a name="input_cache_s3_bucket-region"></a> [cache\_s3\_bucket-region](#input\_cache\_s3\_bucket-region) | The S3 cache's bucket region. | `string` | `""` | no |
| <a name="input_cache_s3_host"></a> [cache\_s3\_host](#input\_cache\_s3\_host) | Object storage host for the S3 cache. | `string` | `""` | no |
| <a name="input_cache_s3_secret-key"></a> [cache\_s3\_secret-key](#input\_cache\_s3\_secret-key) | Secret key for the S3 cache. | `string` | `""` | no |
| <a name="input_cache_s3_secret-name"></a> [cache\_s3\_secret-name](#input\_cache\_s3\_secret-name) | Name of the secret containing the credentials for the S3 cache. | `string` | `""` | no |
| <a name="input_chart_version"></a> [chart\_version](#input\_chart\_version) | Version of the Helm chart used to deploy the GitLab runners. | `string` | `"0.39.0"` | no |
| <a name="input_gitlab_groups"></a> [gitlab\_groups](#input\_gitlab\_groups) | IDs for the GitLab groups to deploy a runner for. A distinct runner is deployed for each group. | `list(number)` | `[]` | no |
| <a name="input_gitlab_projects"></a> [gitlab\_projects](#input\_gitlab\_projects) | IDs for individual GitLab projects to deploy a runner for. A distinct runner is deployed for each project. | `list(number)` | `[]` | no |
| <a name="input_gitlab_runners_name"></a> [gitlab\_runners\_name](#input\_gitlab\_runners\_name) | Name for the GitLab runners. Can be anything, will be shown in GitLab's list of runners. | `string` | `"kubernetes runner"` | no |
| <a name="input_gitlab_url"></a> [gitlab\_url](#input\_gitlab\_url) | GitLab CI coordinator URL. | `string` | `"https://gitlab.com/"` | no |
| <a name="input_kubeconfig"></a> [kubeconfig](#input\_kubeconfig) | Configuration to identify and authenticate with a Kubernetes API server. | <pre>object({<br>    host                   = string<br>    token                  = string<br>    cluster_ca_certificate = string<br>  })</pre> | n/a | yes |
| <a name="input_privileged"></a> [privileged](#input\_privileged) | Allow running privileged containers. Please make sure you understand the security implications. | `bool` | `false` | no |
| <a name="input_project_name"></a> [project\_name](#input\_project\_name) | Project name to label the Kubernetes Namespace. Can be anything. | `string` | n/a | yes |
| <a name="input_project_slug"></a> [project\_slug](#input\_project\_slug) | Slug used for generating various Kubernetes resource names. | `string` | n/a | yes |
| <a name="input_values"></a> [values](#input\_values) | Values to pass through to the Helm releases in a list of YAML formatted text. | `list(string)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_namespace"></a> [namespace](#output\_namespace) | Kubernetes namespace in which the runners were deployed. |
<!-- END_TF_DOCS -->

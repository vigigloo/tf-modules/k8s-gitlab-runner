output "namespace" {
  value = kubernetes_namespace.gitlab_runner.metadata[0].name

  description = "Kubernetes namespace in which the runners were deployed."
}

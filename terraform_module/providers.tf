variable "kubeconfig" {
  type = object({
    host                   = string
    token                  = string
    cluster_ca_certificate = string
  })

  description = "Configuration to identify and authenticate with a Kubernetes API server."
}

provider "kubernetes" {
  host                   = var.kubeconfig.host
  token                  = var.kubeconfig.token
  cluster_ca_certificate = base64decode(var.kubeconfig.cluster_ca_certificate)
}

provider "helm" {
  kubernetes {
    host                   = var.kubeconfig.host
    token                  = var.kubeconfig.token
    cluster_ca_certificate = base64decode(var.kubeconfig.cluster_ca_certificate)
  }
}

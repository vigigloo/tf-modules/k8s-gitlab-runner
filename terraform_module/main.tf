resource "kubernetes_namespace" "gitlab_runner" {
  metadata {
    labels = {
      project = replace(var.project_name, " ", "")
    }

    name = "${var.project_slug}-gitlab-runner"
  }
}

moved {
  from = kubernetes_namespace.gitlab-runner
  to   = kubernetes_namespace.gitlab_runner
}

data "gitlab_project" "project" {
  for_each = toset([for v in var.gitlab_projects : tostring(v)])
  id       = tonumber(each.value)
}

locals {
  secret_name = var.cache_s3_secret-name == null ? "${var.project_slug}-s3-secrets" : var.cache_s3_secret-name
}

resource "kubernetes_secret" "users" {
  count = var.cache_provider == "s3" ? 1 : 0
  metadata {
    name      = local.secret_name
    namespace = kubernetes_namespace.gitlab_runner.metadata[0].name
  }
  data = {
    secretkey = var.cache_s3_secret-key
    accesskey = var.cache_s3_access-key

  }
}

locals {
  s3_cache_config = <<-EOT
    [runners.cache.s3]
      ServerAddress = "${var.cache_s3_host}"
      BucketName = "${var.cache_s3_bucket-name}"
      BucketLocation = "${var.cache_s3_bucket-region}"
      Insecure = false
      AuthenticationType = "access-key"
  EOT
  secret_cache    = <<-EOT
    cache:
      secretName: ${local.secret_name}
  EOT
}

locals {
  config = <<-EOT
    runners:
      config: |
        [[runners]]
          ${indent(2, var.extra_runners_config)}
          [runners.kubernetes]
            namespace = "${kubernetes_namespace.gitlab_runner.metadata[0].name}"
            image = "${var.runner_image}"
            ${indent(4, var.extra_runners_kubernetes_config)}

            [runners.cache]
              Type = "${var.cache_provider}"
              Path = "runner"
              Shared = true
              ${var.cache_provider == "s3" ? indent(4, local.s3_cache_config) : ""}
      ${var.cache_provider != "" ? indent(2, local.secret_cache) : ""}
  EOT
}

resource "gitlab_user_runner" "gitlab_runner_project" {
  for_each    = toset([for v in var.gitlab_projects : tostring(v)])
  runner_type = "project_type"
  project_id  = data.gitlab_project.project[each.value].id
}

moved {
  from = gitlab_user_runner.gitlab-runner-project
  to   = gitlab_user_runner.gitlab_runner_project
}

resource "helm_release" "gitlab_runner_project" {
  for_each   = toset([for v in var.gitlab_projects : tostring(v)])
  name       = "gl-runner-project-${each.value}"
  repository = "https://charts.gitlab.io"
  chart      = "gitlab-runner"
  namespace  = kubernetes_namespace.gitlab_runner.metadata[0].name
  version    = var.chart_version
  values = concat([
    local.config
    ],
  var.values)

  set {
    name  = "runnerToken"
    value = gitlab_user_runner.gitlab_runner_project[each.value].token
  }
  set {
    name  = "rbac.create"
    value = "true"
  }

  set {
    name  = "gitlabUrl"
    value = var.gitlab_url
  }

  set {
    name  = "runners.name"
    value = var.gitlab_runners_name
  }
}

moved {
  from = helm_release.gitlab-runner-project
  to   = helm_release.gitlab_runner_project
}


data "gitlab_group" "group" {
  for_each = toset([for v in var.gitlab_groups : tostring(v)])
  group_id = tonumber(each.value)
}

resource "gitlab_user_runner" "gitlab_runner_group" {
  for_each    = toset([for v in var.gitlab_groups : tostring(v)])
  runner_type = "group_type"
  group_id    = data.gitlab_group.group[each.value].group_id
}

moved {
  from = gitlab_user_runner.gitlab-runner-group
  to   = gitlab_user_runner.gitlab_runner_group
}

resource "helm_release" "gitlab_runner_group" {
  for_each   = toset([for v in var.gitlab_groups : tostring(v)])
  name       = "gl-runner-group-${each.value}"
  repository = "https://charts.gitlab.io"
  chart      = "gitlab-runner"
  namespace  = kubernetes_namespace.gitlab_runner.metadata[0].name
  version    = var.chart_version
  values = concat([
    local.config
    ],
  var.values)

  set {
    name  = "runnerToken"
    value = gitlab_user_runner.gitlab_runner_group[each.value].token
  }

  set {
    name  = "rbac.create"
    value = "true"
  }

  set {
    name  = "gitlabUrl"
    value = var.gitlab_url
  }

  set {
    name  = "runners.name"
    value = var.gitlab_runners_name
  }
}

moved {
  from = helm_release.gitlab-runner-group
  to   = helm_release.gitlab_runner_group
}

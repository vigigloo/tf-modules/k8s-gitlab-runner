variable "project_name" {
  type = string

  description = "Project name to label the Kubernetes Namespace. Can be anything."
}

variable "project_slug" {
  type = string

  description = "Slug used for generating various Kubernetes resource names."
}

variable "gitlab_groups" {
  type    = list(number)
  default = []

  description = "IDs for the GitLab groups to deploy a runner for. A distinct runner is deployed for each group."
}

variable "gitlab_projects" {
  type    = list(number)
  default = []

  description = "IDs for individual GitLab projects to deploy a runner for. A distinct runner is deployed for each project."
}

variable "gitlab_url" {
  type    = string
  default = "https://gitlab.com/"

  description = "GitLab CI coordinator URL."
}

variable "chart_version" {
  type    = string
  default = "0.58.0"

  description = "Version of the Helm chart used to deploy the GitLab runners."
}

variable "gitlab_runners_name" {
  type    = string
  default = "kubernetes runner"

  description = "Name for the GitLab runners. Can be anything, will be shown in GitLab's list of runners."
}

variable "values" {
  type    = list(string)
  default = []

  description = "Values to pass through to the Helm releases in a list of YAML formatted text."
}

variable "cache_provider" {
  type    = string
  default = ""

  validation {
    condition     = contains(["s3", ""], var.cache_provider)
    error_message = "Allowed values for input_parameter are \"s3\", \"\"."
  }

  description = "What provider to use for caching files from CI jobs. Either empty or `s3`."
}

variable "cache_s3_host" {
  type    = string
  default = ""

  description = "Object storage host for the S3 cache."
}
variable "cache_s3_bucket-name" {
  type    = string
  default = ""

  description = "Name of the bucket where the S3 cache should be persisted."
}
variable "cache_s3_bucket-region" {
  type    = string
  default = ""

  description = "The S3 cache's bucket region."
}
variable "cache_s3_secret-name" {
  type    = string
  default = null

  description = "Name of the secret containing the credentials for the S3 cache."
}
variable "cache_s3_access-key" {
  type    = string
  default = ""

  description = "Access key for the S3 cache."
}
variable "cache_s3_secret-key" {
  type    = string
  default = ""

  description = "Secret key for the S3 cache."
}

variable "runner_image" {
  type    = string
  default = "ubuntu:22.04"

  description = "Image in part `runners.kubernetes`"
}

variable "extra_runners_config" {
  type    = string
  default = ""

  description = "Extra parameters to add in `runners` part"
}


variable "extra_runners_kubernetes_config" {
  type    = string
  default = ""

  description = "Extra parameters to add in `runners.kubernetes` part"
}
